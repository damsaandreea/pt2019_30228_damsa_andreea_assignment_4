package main;

import java.util.ArrayList;
import java.util.Date;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerialization;
import presentationLayer.AdministratorGraphicalUserInterface;
import presentationLayer.ChefGrapicalUserInterface;
import presentationLayer.Controller;
import presentationLayer.MainGUI;
import presentationLayer.WaiterGraphicalUserInterface;

public class Main {
	public static void main(String[] args) {
		MenuItem i1=new BaseProduct("Cartofi",10,400,300);
		MenuItem i2=new BaseProduct("Friptura", 25, 500, 600);
		MenuItem i3=new CompositeProduct("Cartofi cu friptura",30,900,900);
		((CompositeProduct)i3).addItem(i1);
		((CompositeProduct)i3).addItem(i2);
		ArrayList<MenuItem> meniu=new ArrayList<MenuItem>();
		meniu.add(i1);
		meniu.add(i2);
		meniu.add(i3);
		RestaurantSerialization ser=new RestaurantSerialization();
		meniu=ser.RestaurantDeserIN();
		ChefGrapicalUserInterface chef=new ChefGrapicalUserInterface();
		Restaurant r=new Restaurant(meniu,chef);
		Order o=new Order(new Date(),1);
		ArrayList<MenuItem> comanda=new ArrayList<MenuItem>();
		comanda.add(i1);
		comanda.add(i3);
		//r.createNewOrder(o, comanda);
		//r.generateBill(o);
		//MainGUI v=new MainGUI();
		AdministratorGraphicalUserInterface a=new AdministratorGraphicalUserInterface(meniu); 
		WaiterGraphicalUserInterface w=new WaiterGraphicalUserInterface(r);
		MainGUI main=new MainGUI();
		Controller c=new Controller(main,a,w,r);
	}
}
