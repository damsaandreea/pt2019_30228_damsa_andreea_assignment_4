package dataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import businessLayer.MenuItem;

public class RestaurantSerialization {
	ObjectOutputStream out;
	ObjectInputStream in;
	String filename="restaurant.ser";
	public void RestaurantSerOUT(ArrayList<MenuItem> a) {
		try {
		FileOutputStream file=new FileOutputStream(filename);	
		out=new ObjectOutputStream(file);
		out.writeObject(a);
	    out.close();
	    file.close();
		}catch(Exception e) {
	    	   System.out.println(e.getMessage());
	       }
	}
	
	public ArrayList<MenuItem> RestaurantDeserIN(){
		try {
		FileInputStream file=new FileInputStream(filename);
		in=new ObjectInputStream(file);
		ArrayList<MenuItem> object1=(ArrayList<MenuItem>)in.readObject();
		in.close();
		file.close();
		return object1;}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}


}
