package businessLayer;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public abstract class MenuItem implements Serializable{
	private String nume;
	protected int pret;
	protected int calorii;
	protected int gramaj;
	public MenuItem() {
		
	}
	public MenuItem(String nume, int pret, int calorii, int gramaj) {
		this.nume=nume;
		this.pret=pret;
		this.calorii=calorii;
		this.gramaj=gramaj;
	}
	public abstract int computePrice();
	public abstract int computeCalories();
	public abstract int computeGramaj();
	public String toString() {
		return this.nume;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public int getPret() {
		return pret;
	}
	public void setPret(int pret) {
		this.pret = pret;
	}
	public int getCalorii() {
		return calorii;
	}
	public void setCalorii(int calorii) {
		this.calorii = calorii;
	}
	public int getGramaj() {
		return gramaj;
	}
	public void setGramaj(int gramaj) {
		this.gramaj = gramaj;
	}
	
}
