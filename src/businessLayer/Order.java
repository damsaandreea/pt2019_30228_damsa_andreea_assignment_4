package businessLayer;

import java.util.Date;

public class Order {
	//aici trebuie sa definesc hash codeul si equals ul
	private static int index=0;
	private int orderID;
	private Date data;
	private int masa;
	private int cost;
	public Order(Date data, int masa) {
		this.orderID=index++;
		this.data=data;
		this.masa=masa;
	}
	@Override
	public int hashCode() {
		int prime=7;
		int result=1;
		result=prime*result+ orderID;
		result=prime*result+ (this.data==null ? 0: this.data.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object o) {
		if (o==this) return true;
		Order other=(Order)o;
		if (this.getOrderID()!=other.getOrderID()) return false;
		return true;
	}
	public void setCost(int cost) {
		this.cost=cost;
	}
	public int getCost() {
		return this.cost;
	}
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public int getMasa() {
		return masa;
	}
	public void setMasa(int masa) {
		this.masa = masa;
	}
	
	
}
