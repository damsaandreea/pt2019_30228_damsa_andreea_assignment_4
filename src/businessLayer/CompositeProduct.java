package businessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem {
	private List<MenuItem> continut;
	public CompositeProduct(String nume, int pret, int calorii, int gramaj) {
		super(nume, pret,calorii, gramaj);
		continut=new ArrayList<MenuItem>();
	}
	@Override
	public int computePrice() {
		int finalSum=0;
		for (MenuItem item: continut) {
			finalSum+=item.computePrice();
		}
		super.pret=finalSum;
		return finalSum;
	}
	public int computeCalories() {
		int finalCal=0;
		for (MenuItem item: continut) {
			finalCal+=item.getCalorii();
		}
		super.calorii=finalCal;
		return finalCal;
	}
	public int computeGramaj() {
		int finalGram=0;
		for (MenuItem item: continut) {
			finalGram+=item.getGramaj();
		}
		super.gramaj=finalGram;
		return finalGram;
	}
	public void addItem(MenuItem item) {
		continut.add(item);
	}
	public void removeItem(MenuItem item) {
		continut.remove(item);
	}
}
