package businessLayer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import dataLayer.RestaurantSerialization;
import presentationLayer.ChefGrapicalUserInterface;

public class Restaurant extends Observable implements RestaurantProcessing {
	ArrayList<MenuItem> meniu;
	RestaurantSerialization ser=new RestaurantSerialization();
	ChefGrapicalUserInterface c;
	Map<Order,List<MenuItem>> comenzi=new HashMap<Order,List<MenuItem>>();
	public Restaurant(ArrayList<MenuItem> meniu, ChefGrapicalUserInterface c) {
		this.c=c;
		this.meniu=meniu;
		
	}
	public Map<Order,List<MenuItem>> getOrders(){
		return this.comenzi;
	}
	public void createNewMenuItem(MenuItem item) {
		assert item!=null;
		int size=meniu.size();
		meniu.add(item);
		assert meniu.size()==size+1;
		ser.RestaurantSerOUT(meniu);
	}
	public void deleteMenuItem(String nume) {
		assert nume!=null;
		int size=meniu.size();
		assert size>0;
		for (Iterator<MenuItem> iterator=meniu.iterator();iterator.hasNext();) {
			MenuItem it=iterator.next();
			if (it.getNume().equals(nume)) {
				iterator.remove();
			}
		}
		assert meniu.size()==size-1;
		ser.RestaurantSerOUT(meniu);
	}
	public void editMenuItem(MenuItem item) {
		assert item!=null;
		int size=meniu.size();
		Iterator<MenuItem> it=meniu.iterator();
		while (it.hasNext()) {
			MenuItem i=(MenuItem)it.next();
			if (i.getNume().equals(item.getNume())) {
					i=item;
					}
		}
		assert meniu.size()==size;
		ser.RestaurantSerOUT(meniu);
	}
	public ArrayList<MenuItem> getMeniu(){
		return meniu;
	}
	public void setMeniu(ArrayList<MenuItem> meniu) {
		this.meniu=meniu;
	}
	public void createNewOrder(Order o, List<MenuItem> lista) {
		//aici ar tebui anuntat chef-ul(functioneaza ca si un notify)
		assert o!=null;
		assert lista!=null;
		int size=comenzi.size();
		boolean notify=false;
		for (MenuItem item:lista) {
			if (item instanceof CompositeProduct)
				notify=true;
		}
		if (notify==true) {
		c.update(this, lista);}
		comenzi.put(o, lista);
		assert comenzi.size()==size+1;
	}
	public int computePriceOrder(Order o) {
		assert o!=null;
		List<MenuItem> comanda=comenzi.get(o);
		int finalSum=0;
		for (MenuItem item: comanda) {
			finalSum+=item.getPret();
		}
		o.setCost(finalSum);
		assert finalSum>0;
		return finalSum;
		
	}
	public void generateBill(Order o) {
		assert o!=null;
		List<MenuItem> comanda=comenzi.get(o);
		String numeComanda="Comanda"+o.getOrderID()+".txt";
		try {
			PrintWriter writer=new PrintWriter(numeComanda,"UTF-8");
			writer.println("Consum:");
			int finalBill=0;
			for (MenuItem item: comanda) {
				writer.println(item.getNume()+" : "+item.getPret()+" lei;");
				finalBill+=item.getPret();
			}
			writer.println("Nota de plata: "+finalBill+" lei.");
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
			
	}
	protected boolean isWellFormed() {
		if (meniu.size()<0)
			return false;
		if (comenzi.size()<0)
			return false;
		Set<Order> keys=comenzi.keySet();
		if (keys.size()!=comenzi.size())
			return false;
		return true;
	}

}
