package businessLayer;

import java.util.ArrayList;
import java.util.List;

public interface RestaurantProcessing {
	/**
	 * adds a new item in the menu
	 * @pre item!=null
	 * @post getSize()==getSize()@pre + 1
	 * @param item
	 */
	public void createNewMenuItem(MenuItem item);
	
	/**
	 * delets an item from the menu
	 * @pre nume!=null
	 * @pre getSize()>0
	 * @post getSize()==getSize()@pre-1
	 * @param nume
	 */
	public void deleteMenuItem(String nume);
	
	/**
	 * edits an item from the menu
	 * @pre item!=null
	 * @post getSize()==getSize()@pre 
	 * @param item
	 */
	public void editMenuItem(MenuItem item);
	
	/**
	 * @pre o!=null
	 * @pre lista!=null
	 * @post getSize()==getSize()@pre+1
	 * @param o
	 * @param lista
	 */
	public void createNewOrder(Order o, List<MenuItem> lista);
	
	/**
	 * @pre o!=null
	 * @post price>0
	 * @param o
	 * @return
	 */
	public int computePriceOrder(Order o);
	
	/**
	 * @pre o!=null
	 * @post @nochange
	 * @param o
	 */
	public void generateBill(Order o);
}
