package businessLayer;

public class BaseProduct extends MenuItem {
	public BaseProduct(String nume, int pret, int calorii, int gramaj) {
		super(nume, pret, calorii, gramaj);
	}
	public int computePrice() {
		return pret;
	}
	public int computeCalories() {
		return calorii;
	}
	public int computeGramaj() {
		return gramaj;
	}
}
