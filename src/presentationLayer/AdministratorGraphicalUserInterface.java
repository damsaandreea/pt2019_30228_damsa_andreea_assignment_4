package presentationLayer;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.*;

import businessLayer.MenuItem;
public class AdministratorGraphicalUserInterface extends JFrame {
	JButton b1=new JButton("Creaza fel nou");
	JButton b2=new JButton("Sterge fel");
	JButton b3=new JButton("Editeaza fel");
	JTextField nume=new JTextField(20);
	JTextField pret=new JTextField("0");
	JTextField calorii=new JTextField("0");
	JTextField gramaj=new JTextField("0");
	JButton b4= new JButton("OK");
	JButton b5=new JButton("Arata meniu detaliat");
	JList op;
	ArrayList<MenuItem> iteme;
	DefaultListModel<String> listModel;
	public AdministratorGraphicalUserInterface(ArrayList<MenuItem> iteme) {
		this.iteme=iteme;
		JPanel p=new JPanel();
		p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));
		pret.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				pret.setText("");
			}
			public void focusLost(FocusEvent e) {
				
			}
		});
		calorii.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				calorii.setText("");
			}
			public void focusLost(FocusEvent e) {
				
			}
		});
		gramaj.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				gramaj.setText("");
			}
			public void focusLost(FocusEvent e) {
				
			}
		});
		p.add(new JLabel("Nume:"));
		p.add(nume);
		p.add(new JLabel("Pret:"));
		p.add(pret);
		p.add(new JLabel("Calorii:"));
		p.add(calorii);
		p.add(new JLabel("Gramaj:"));
		p.add(gramaj);
		JPanel butoane=new JPanel();
		butoane.setLayout(new BoxLayout(butoane, BoxLayout.Y_AXIS));
		butoane.add(new JLabel("Operatii:"));
		butoane.add(b1);
		butoane.add(b2);
		butoane.add(b3);
		butoane.add(b5);
		JPanel total=new JPanel();
		total.add(p);
		total.add(butoane);
		op= new  JList<>(iteme.toArray()); 
		op.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		JScrollPane sp=new JScrollPane(op);
		p.add(new JLabel("Alege componentele felului compus creat:"));
		p.add(sp);
		JPanel jos=new JPanel();
		jos.add(b4);
		JPanel totalFinal=new JPanel();
		totalFinal.setLayout(new BoxLayout(totalFinal, BoxLayout.Y_AXIS));
		totalFinal.add(total);
		totalFinal.add(jos);
		this.setContentPane(totalFinal); 
		this.setSize(700, 550);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Administrator");
		
	}
	/*public void refreshOptions() {
		String[] choices=new String[iteme.size()];
		int index=0;
		for (MenuItem item:iteme) {
			choices[index++]=item.getNume();
		}
		listModel.clear();
		for (int i=0; i<choices.length;i++) {
			listModel.addElement(choices[i]);
		}
		 
	}*/
	public void addB1Listener(ActionListener e) {
		b1.addActionListener(e);
	}
	public void addB2Listener(ActionListener e) {
		b2.addActionListener(e);
	}
	public void addB3Listener(ActionListener e) {
		b3.addActionListener(e);
	}
	public void addB4Listener(ActionListener e) {
		b4.addActionListener(e);
	}
	public void addB5Listener(ActionListener e){
		b5.addActionListener(e);
	}
	public String getNume() {
		return nume.getText();
	}
	public String getCalorii() {
		return calorii.getText();
	}
	public String getGramaj() {
		return gramaj.getText();
	}
	public String getPret() {
		return pret.getText();
	}
	public void setNume(String s) {
		nume.setText(s);
	}
	public void setPret(String s) {
		pret.setText(s);
	}
	public void setCalorii(String s) {
		calorii.setText(s);
	}
	public void setGramaj(String s) {
		gramaj.setText(s);
	}
	public void setInvisible(){
		this.setVisible(false);
		}
	public void setVisible() {
		this.setVisible(true);
	}
	public int[] getSelected() {
		return op.getSelectedIndices();
	}
	public JList getOp() {
		return op;
	}
	public void setOp(JList op) {
		this.op=op;
	}
}
