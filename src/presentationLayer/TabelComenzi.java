package presentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import presentationLayer.TabelMeniu.OKActionListener;

public class TabelComenzi {
	private Restaurant r;
	JFrame t=new JFrame();
	JButton ok=new JButton("OK");
	public TabelComenzi(Restaurant r) {
		this.r=r;
		Set<Order> lista=r.getOrders().keySet();
		String[] column= {"Id", "Masa","Data"};
		int size=lista.size();
		String[][] data= new String[size][20];
		int index=0;
		if (lista!=null) {
		for( Order m:lista ) {
			data[index][0]=Integer.toString(m.getOrderID());
			data[index][1]=Integer.toString(m.getMasa());
			data[index][2]=m.getData().toString();
			
			index++;
		}}
		JTable table=new JTable(data,column);    
	    table.setBounds(60,100,900,500);      
	    JScrollPane sp=new JScrollPane(table);
	    sp.setSize(600,600);
	    t.setSize(600, 600);
	    JPanel content=new JPanel();
	    content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		content.add(sp);
		content.add(ok);
		ok.addActionListener(new OKActionListener());
		t.setContentPane(content);
		t.setSize(500,400);
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		t.setTitle("Produse");
		t.setVisible(true);
	}
	public class OKActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			t.setVisible(false);
		}
	}
}
