package presentationLayer;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MainGUI extends JFrame{
	JButton a=new JButton("Administrator");
	JButton w=new JButton("Chelner");
	JLabel img=new JLabel();
	public MainGUI() {
		JPanel p=new JPanel();
		p.setLayout(new BorderLayout());
		ImageIcon icon=new ImageIcon("C:\\Users\\damsa\\eclipse-workspace\\Restaurant\\Images\\res.jpg");
		img.setIcon(icon);
		p.add(img,BorderLayout.CENTER);
		JPanel jos=new JPanel();
		a.setPreferredSize(new Dimension(200,100));
		a.setFont(new Font("Arial", Font.PLAIN, 22));
		w.setFont(new Font("Arial", Font.PLAIN, 22));
		w.setPreferredSize(new Dimension(200,100));
		jos.add(a);
		jos.add(w);
		p.add(jos,BorderLayout.PAGE_END);
		//p.add(w,BorderLayout.PAGE_END);
		this.setContentPane(p);
		this.setVisible(true); 
		this.setSize(550, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Restaurant");
		
	}
	public void addWListener(ActionListener e) {
		w.addActionListener(e);
	}
	public void addAListener(ActionListener e) {
		a.addActionListener(e);
	}
	public void setInvisible() {
		this.setVisible(false);
	}
	public void setVisible() {
		this.setVisible(true);
	}

}
