package presentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class Controller {
	private MainGUI main;
	private AdministratorGraphicalUserInterface admin;
	private WaiterGraphicalUserInterface waiter;
	private Restaurant r;
	public Controller(MainGUI main,AdministratorGraphicalUserInterface admin,WaiterGraphicalUserInterface waiter,Restaurant r) {
		this.main=main;
		this.admin=admin;
		this.waiter=waiter;
		this.r=r;
		this.main.addAListener(new AListener());
		this.main.addWListener(new WListener());
		this.admin.addB4Listener(new B4Listener());
		this.admin.addB1Listener(new B1Listener());
		this.admin.addB2Listener(new B2Listener());
		this.admin.addB3Listener(new B3Listener());
		this.admin.addB5Listener(new B5Listener());
		this.waiter.addB1Listener(new WB1Listener());
		this.waiter.addB2Listener(new WB2Listener());
		this.waiter.addB3Listener(new WB3Listener());
		this.waiter.addB4Listener(new WB4Listener());
		this.waiter.addB5Listener(new WB5Listener());
	}
	public class AListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
				main.setInvisible();
				admin.setVisible();
		}
	}
	public class WListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			main.setInvisible();
			waiter.setVisible();
		}
	}
	public class B4Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			admin.setInvisible();
			main.setVisible();
		}
	}
	public class B1Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String nume=admin.getNume();
			int pret=Integer.parseInt(admin.getPret());
			int calorii=Integer.parseInt(admin.getCalorii());
			int gramaj=Integer.parseInt(admin.getGramaj());
			MenuItem nou;
			if (pret==0) {
				nou=new CompositeProduct(nume,pret,calorii,gramaj);
				List<MenuItem> choices=admin.getOp().getSelectedValuesList();
				for (MenuItem item:choices) {
					((CompositeProduct)nou).addItem(item);
				}
				 nou.computePrice();
				 nou.computeCalories();
				 nou.computeGramaj();
			}else {
				nou= new BaseProduct(nume,pret,calorii,gramaj);
			}
			r.createNewMenuItem(nou);
			//pt update
			admin.getOp().setListData(r.getMeniu().toArray());
			//System.out.println(nou.toString());
			//admin.refreshOptions();
			/*for (MenuItem item: r.getMeniu()) {
				System.out.println(item.toString());
			}*/
			admin.setPret("0");
			admin.setCalorii("0");
			admin.setGramaj("0");
			admin.setNume("");
		}
	}
	public class B2Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			//stergere
			String nume=admin.getNume();
			r.deleteMenuItem(nume);
 			admin.getOp().setListData(r.getMeniu().toArray());
			//admin.refreshOptions();
		}
	}
	public class B3Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String nume=admin.getNume();
			int pret=Integer.parseInt(admin.getPret());
			int calorii=Integer.parseInt(admin.getCalorii());
			int gramaj=Integer.parseInt(admin.getGramaj());
			Iterator<MenuItem> it=r.getMeniu().iterator();
			while (it.hasNext()) {
				MenuItem gasit=it.next();
				if (gasit.getNume().equals(nume)){
					gasit.setCalorii(calorii);
					gasit.setGramaj(gramaj);
					gasit.setPret(pret);
				}
			}
		}
	}
	public class B5Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			TabelMeniu tabel=new TabelMeniu(r);
		}
	}
	public class WB4Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			main.setVisible();
			waiter.setInvisible();
		}
	}
	public class WB1Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int masa=Integer.parseInt(waiter.getMasa());
			Order o=new Order(new Date(),masa);
			List<MenuItem> choices=waiter.getOp().getSelectedValuesList();
			 r.createNewOrder(o, choices);
		}
	}
	
	public class WB2Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Map<Order,List<MenuItem>> comenzi=r.getOrders();
			int id=Integer.parseInt(waiter.getID());
			Order o=null;
			for (Order key:comenzi.keySet()) {
				if (key.getOrderID()==id)
					o=key;
			}
			int price=r.computePriceOrder(o);
			waiter.setTotal(Integer.toString(price));
		}
	}
	public class WB3Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			
		}
	}
	public class WB5Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			TabelComenzi tc=new TabelComenzi(r);
		}
	}
	
}
