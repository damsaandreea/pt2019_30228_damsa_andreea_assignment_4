package presentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.MenuItem;
import businessLayer.Restaurant;



public class TabelMeniu  {
	private Restaurant r;
	JFrame t=new JFrame();
	JButton ok=new JButton("OK");
	public TabelMeniu(Restaurant r) {
		this.r=r;
		ArrayList<MenuItem> lista=this.r.getMeniu();
		String[] column= {"Nume", "Pret","Calorii", "Gramaj"};
		int size=lista.size();
		String[][] data= new String[size][20];
		int index=0;
		if (lista!=null) {
		for( MenuItem m:lista ) {
			data[index][0]=m.getNume();
			data[index][1]=Integer.toString(m.getPret());
			data[index][2]=Integer.toString(m.getCalorii());
			data[index][3]=Integer.toString(m.getGramaj());
			index++;
		}}
		JTable table=new JTable(data,column);    
	    table.setBounds(60,100,900,500);      
	    JScrollPane sp=new JScrollPane(table);
	    sp.setSize(600,600);
	    t.setSize(600, 600);
	    JPanel content=new JPanel();
	    content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		content.add(sp);
		content.add(ok);
		ok.addActionListener(new OKActionListener());
		t.setContentPane(content);
		t.setSize(500,400);
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		t.setTitle("Produse");
		t.setVisible(true);
	}
	public class OKActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			t.setVisible(false);
		}
	}
}
