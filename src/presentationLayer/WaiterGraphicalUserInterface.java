package presentationLayer;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import businessLayer.MenuItem;
import businessLayer.Restaurant;

public class WaiterGraphicalUserInterface extends JFrame {
	JButton b1=new JButton("Creaza comanda");
	JButton b2=new JButton("Genereaza total plata");
	JButton b3=new JButton ("Genereaza nota");
	JButton b4=new JButton("OK");
	JButton b5=new JButton("Afiseaza toate comenzile");
	JTextField masa=new JTextField(5);
	JTextField id=new JTextField(5);
	JTextField total=new JTextField(5);
	JList op;
	Restaurant r;

	public WaiterGraphicalUserInterface(Restaurant r) {
	//va trebui sa aiba iarasi un list cu ce s-a comandat
	this.r=r;
	JPanel principal=new JPanel();
	op= new  JList<>(r.getMeniu().toArray()); 
	op.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	JScrollPane sp=new JScrollPane(op);
	principal.add(new JLabel("Masa:"));
	principal.add(masa);
	principal.add(new JLabel("ID comanda(pentru nota de plata):"));
	principal.add(id);
	principal.add(new JLabel("Data comenzii va fi cea a apasarii butonului"));
	principal.add(new JLabel("Alege felurile comandate:"));
	principal.add(sp);
	principal.add(new JLabel("Totalul de plata este:"));
	principal.add(total);
	principal.setLayout(new BoxLayout(principal,BoxLayout.Y_AXIS));
	JPanel butoane=new JPanel();
	butoane.setLayout(new BoxLayout(butoane,BoxLayout.Y_AXIS));
	butoane.add(b1);
	butoane.add(b2);
	butoane.add(b3);
	butoane.add(b5);
	JPanel jos=new JPanel();
	jos.add(b4);
	JPanel sus=new JPanel();
	sus.add(principal);
	sus.add(butoane);
	JPanel total=new JPanel();
	total.setLayout(new BoxLayout(total,BoxLayout.Y_AXIS));
	total.add(sus);
	total.add(jos);
	this.setContentPane(total);
	this.setSize(700, 550);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setTitle("Chelner");
	}

	public String getMasa() {
		return this.masa.getText();
	}
	public String getID() {
		return id.getText();
	}
	public void addB1Listener(ActionListener e) {
		b1.addActionListener(e);
	}
	public void addB2Listener(ActionListener e) {
		b2.addActionListener(e);
	}
	public void addB3Listener(ActionListener e) {
		b3.addActionListener(e);
	}
	public void addB4Listener(ActionListener e) {
		b4.addActionListener(e);
	}
	public void addB5Listener(ActionListener e) {
		b5.addActionListener(e);
	}
	public void setInvisible(){
		this.setVisible(false);
		}
	public void setVisible() {
		this.setVisible(true);
	}
	public int[] getSelected() {
		return op.getSelectedIndices();
	}
	public void setOp(JList op) {
		this.op=op;
	}
	public JList getOp() {
		return op;
	}
	public void setTotal(String total) {
		this.total.setText(total);
	}
}
